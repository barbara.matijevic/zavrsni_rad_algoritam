﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp9
{
    public partial class Form0 : System.Windows.Forms.Form
    {
        
        static String igrac1; // koristimo static, jer se jedino one mogu izmjenjivati među formama
        static bool red1;   // metoda koja je ista za svaki objekt dane klase 
        static String ime;
 

        public Form0()
        {
            InitializeComponent();

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            var tx_box = sender as TextBox;  //preuzimamo text iz textBoxa
            ime = tx_box.Text;

        }

        private void btn_O_clck(object sender, EventArgs e)
        {
            igrac1 = "O";
        }

        private void btn_X_clck(object sender, EventArgs e)
        {
            igrac1 = "X"; 
        }

        private void drugi_btn_Click(object sender, EventArgs e)
        {
            red1 = false;
            Form1 frm = new Form1(igrac1, red1, ime);
            frm.Show();
            this.Hide();
        }

        private void prvi_btn_Click(object sender, EventArgs e)
        {
            red1 = true;
            Form1 frm = new Form1(igrac1, red1, ime);
            frm.Show();
            this.Hide();
        }

        
    }
}
