﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp9
{
    public partial class Form1 : System.Windows.Forms.Form
    {

        //riješeno u seminaru.
        bool pv;  //privremena varijabla //tko je sada na potezu
        bool red;
        bool sljedeci; //tko je slijedeći na potezu

        int i, j;       
        int brojac = 0;  // brojač odigranih pokreta
        int suma = 0;

        private int[,] matrica = new int[3, 3];


        public Form1(String igr1, bool red1, String ime)    // Iz Form0 primamo simbol kojim igrač želi igrati 
        {
            InitializeComponent();
            brojac = 0;
            btn00.Text = btn01.Text = btn02.Text = btn10.Text = 
            btn11.Text = btn12.Text = btn20.Text = btn21.Text = btn22.Text = ""; //postavljanje gumba a početnu vrijednost
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrica[i, j] = -1;
                }
            }      

            lbl_1.Text = ime; 
            if (igr1 == "X" && red1 == true ) { pv = true; red = true;  }
            if (igr1 == "X" && red1 == false) { pv = false; red = false;  pv=RacunaloIgra(pv); brojac = brojac - 1; }
            if (igr1 == "O" && red1 == true)  { pv = false; red = true;  }
            if (igr1 == "O" && red1 == false) { pv = true; red = false; pv=RacunaloIgra(pv); brojac = brojac - 1; }       
        }

        //izbornik
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NovaIgra();
        }  // Nova Igra
        void NovaIgra()

        {

            if (red == true)
            {
                brojac = 0;
                btn00.Text = btn01.Text = btn02.Text = btn10.Text = btn11.Text = btn12.Text = btn20.Text = btn21.Text = btn22.Text = "";
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        matrica[i, j] = -1;
                    }
                }
            }
            else
            {
                brojac = 0;
                btn00.Text = btn01.Text = btn02.Text = btn10.Text = btn11.Text = btn12.Text = btn20.Text = btn21.Text = btn22.Text = "";
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        matrica[i, j] = -1;
                    }
                }
                if (pv == true) { pv = false; }
                else { pv = true;}
                
                pv = RacunaloIgra(pv);
                brojac = brojac - 1;

            }
           
            
        }     //metoda nova igra
        private void oIgriToolStripMenuItem_Click(object sender, EventArgs e)   // O Igri
        {
            MessageBox.Show("TicTacToe je igra predviđena za dva igrača. Jedan igrač je X, a drugi je O." +
                " Svaki igrač postavlja svoje znakove u slobodna polja. Cilj " +
                "igre je spojiti tri ista znaka vodoravno, uspravno ili dijagonalno, " +
                "kako bi ostvarili pobijedu. ", "O Igri");
        }
        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)   // Izlaz iz igre 
        {
            this.Close();
        }



        void ProvjeriBrojac()
        {
            if (brojac == 8 && red == false)
            {
                MessageBox.Show("Oppps, neriješeno", "Kraj igre");
                NovaIgra();
            }
            if (brojac == 9)
            {
                MessageBox.Show("Oppps, neriješeno", "Kraj igre");
                NovaIgra();
            }

        }

        //metoda za provjeru pobijeda 
        void Pobjeda()
        {
            //horizontalna provjera

            

            if ((matrica[0, 0] == 1 && matrica[0, 1] == 1 && matrica[0, 2] == 1)
                || (matrica[1, 0] == 1 && matrica[1, 1] == 1 && matrica[1, 2] == 1)
                || (matrica[2, 0] == 1 && matrica[2, 1] == 1 && matrica[2, 2] == 1))
            {
                MessageBox.Show("Pobijedio je X", "Pobjeda");
                NovaIgra();
            }

            if   ((matrica[0, 0] == 2 && matrica[0, 1] == 2 && matrica[0, 2] == 2)
               || (matrica[1, 0] == 2 && matrica[1, 1] == 2 && matrica[1, 2] == 2)
               || (matrica[2, 0] == 2 && matrica[2, 1] == 2 && matrica[2, 2] == 2))
            {
                MessageBox.Show("Pobijedio je 0", "Pobjeda");
                NovaIgra();
            }

            //vertikalna provjera 

            if ((matrica[0, 0] == 1 && matrica[1, 0] == 1 && matrica[2, 0] == 1)
                || (matrica[0, 1] == 1 && matrica[1, 1] == 1 && matrica[2, 1] == 1)
                || (matrica[0, 2] == 1 && matrica[1, 2] == 1 && matrica[2, 2] == 1))
            {
                MessageBox.Show("Pobijedio je X", "Pobjeda");
                NovaIgra();

            }

            if ((   matrica[0, 0] == 2 && matrica[1, 0] == 2 && matrica[2, 0] == 2)
                || (matrica[0, 1] == 2 && matrica[1, 1] == 2 && matrica[2, 1] == 2)
                || (matrica[0, 2] == 2 && matrica[1, 2] == 2 && matrica[2, 2] == 2))
            {
                MessageBox.Show("Pobijedio je O", "Pobjeda");
                NovaIgra();

            }

            //dijagonale 
            if  ((matrica[0, 0] == 1 && matrica[1, 1] == 1 && matrica[2, 2] == 1)
                || (matrica[2, 0] == 1 && matrica[1, 1] == 1 && matrica[0, 2] == 1))
            {
                MessageBox.Show("Pobijedio je X", "Pobjeda");
                NovaIgra();

            }

            if   ((matrica[0, 0] == 2 && matrica[1, 1] == 2 && matrica[2, 2] == 2)
               || (matrica[2, 0] == 2 && matrica[1, 1] == 2 && matrica[0, 2] == 2))
            {
                MessageBox.Show("Pobijedio je O", "Pobjeda");
                NovaIgra();

            }

        }   

        //na klik gumba, gumbu se dodijeljuje tekst x ili o, ovisno o odabranom simbolu
        private void btn00_Click(object sender, EventArgs e)
        {
            if (btn00.Text == "")
            {
                if (pv == true) {  clck(true, 0, 0); pv = false; pv=RacunaloIgra(pv); }
                else { clck(false, 0, 0); pv = true; pv=RacunaloIgra(pv); }
                Pobjeda(); ProvjeriBrojac();


            }  
        }
        private void btn01_Click(object sender, EventArgs e)
        {
            if (btn01.Text == "")
            {
                if (pv == true) { clck(true, 0, 1); pv = false; pv =RacunaloIgra(pv);  }
                else { clck(false, 0, 1); pv = true;   pv=RacunaloIgra(pv);  }
                Pobjeda(); ProvjeriBrojac();
            }
        }
        private void btn02_Click(object sender, EventArgs e)
        {
            if (btn02.Text == "")
            {
                if (pv == true) { clck(true, 0, 2); pv = false; pv=RacunaloIgra(pv); }
                else { clck(false, 0, 2); pv = true; pv=RacunaloIgra(pv); }
                Pobjeda(); ProvjeriBrojac();
            }
        }
        private void btn10_Click(object sender, EventArgs e)
        {
            if (btn10.Text == "")
            {
                if (pv == true) { clck(true, 1, 0); pv = false; pv =RacunaloIgra(pv); }
                else { clck(false, 1, 0); pv = true; pv  =RacunaloIgra(pv); }
                Pobjeda(); ProvjeriBrojac();

            }
        }
        private void btn11_Click(object sender, EventArgs e)
        {
            if(btn11.Text=="")
            {
                if (pv == true) { clck(true, 1, 1); pv= false; pv =RacunaloIgra(pv); }
                else { clck(false, 1, 1); pv = true; pv=RacunaloIgra(pv);}
                Pobjeda(); ProvjeriBrojac();

            }
            
        }
        private void btn12_Click(object sender, EventArgs e)
        {
            if (btn12.Text == "")
            {
                if (pv == true) { clck(true, 1, 2); pv = false; pv =RacunaloIgra(pv); }
                else { clck(false, 1, 2); pv = true; pv =RacunaloIgra(pv);  }
                Pobjeda(); ProvjeriBrojac();
            }
            
            
        }
        private void btn20_Click(object sender, EventArgs e)
        {
            if (btn20.Text == "")
            {
                if (pv == true) { clck(true, 2, 0); pv = false; pv=RacunaloIgra(pv);  }
                else { clck(false, 2, 0); pv = true; pv =RacunaloIgra(pv); }
                Pobjeda(); ProvjeriBrojac();
            }

        }
        private void btn21_Click(object sender, EventArgs e)
        {
            {
                if (btn21.Text == "")
                {
                    if (pv == true) { clck(true, 2, 1); pv = false; pv=RacunaloIgra(pv);  }
                    else { clck(false, 2, 1); pv = true; pv =RacunaloIgra(pv);}
                    Pobjeda(); ProvjeriBrojac();
                }
            }
        }
        private void btn22_Click(object sender, EventArgs e)
        {
            {
                Button b = (Button)sender;
                if (btn22.Text == "")
                {
                    if (pv == true) { clck(true, 2, 2); pv = false; pv=RacunaloIgra(pv);  }
                    else { clck(false, 2, 2); pv = true; pv =RacunaloIgra(pv);}
                    Pobjeda(); ProvjeriBrojac();
                }
            }
        } 

        //Funkcija koja se poziva klikom na gumb  ili kada računalo igra slijedeći pokret 
        void clck(bool pv, int i, int j)
        {
            if (pv == true)
            {
                if (i == 0 && j == 0) { matrica[0, 0] = 1; Button b = btn00; b.Text = "X"; }
                if (i == 0 && j == 1) { matrica[0, 1] = 1; Button b = btn01; b.Text = "X"; }
                if (i == 0 && j == 2) { matrica[0, 2] = 1; Button b = btn02; b.Text = "X"; }
                if (i == 1 && j == 0) { matrica[1, 0] = 1; Button b = btn10; b.Text = "X"; }
                if (i == 1 && j == 1) { matrica[1, 1] = 1; Button b = btn11; b.Text = "X"; }
                if (i == 1 && j == 2) { matrica[1, 2] = 1; Button b = btn12; b.Text = "X"; }
                if (i == 2 && j == 0) { matrica[2, 0] = 1; Button b = btn20; b.Text = "X"; }
                if (i == 2 && j == 1) { matrica[2, 1] = 1; Button b = btn21; b.Text = "X"; }
                if (i == 2 && j == 2) { matrica[2, 2] = 1; Button b = btn22; b.Text = "X"; }
                brojac++;              

            }

            if (pv == false)
            {
                if (i == 0 && j == 0) { matrica[0, 0] = 2; Button b = btn00; b.Text = "O"; }
                if (i == 0 && j == 1) { matrica[0, 1] = 2; Button b = btn01; b.Text = "O"; }
                if (i == 0 && j == 2) { matrica[0, 2] = 2; Button b = btn02; b.Text = "O"; }
                if (i == 1 && j == 0) { matrica[1, 0] = 2; Button b = btn10; b.Text = "O"; }
                if (i == 1 && j == 1) { matrica[1, 1] = 2; Button b = btn11; b.Text = "O"; }
                if (i == 1 && j == 2) { matrica[1, 2] = 2; Button b = btn12; b.Text = "O"; }
                if (i == 2 && j == 0) { matrica[2, 0] = 2; Button b = btn20; b.Text = "O"; }
                if (i == 2 && j == 1) { matrica[2, 1] = 2; Button b = btn21; b.Text = "O"; }
                if (i == 2 && j == 2) { matrica[2, 2] = 2; Button b = btn22; b.Text = "O"; }
                brojac++;
            }
        }



        public bool RacunaloIgra(bool pv)
        {

            if (pv == true)
            {
                sljedeci = false;

                if (brojac == 0) { clck(true, 1, 1); return sljedeci; } //ako prvo igra računalo

                if (brojac == 1) 
                {
                    //racunalo započinje igru
                    if (red== true && matrica[1, 1] != 2) { clck(true, 1, 1); return sljedeci; }

                    if ( matrica[0, 0] == 2) { clck(true, 2, 2); return sljedeci; }
                    if (matrica[2, 2] == 2) { clck(true, 0, 0); return sljedeci; }
                    if (matrica[0, 2] == 2) { clck(true, 2, 0); return sljedeci; }
                    if ( matrica[2, 0] == 2) { clck(true, 0, 2); return sljedeci; }

                    // računalo igra kao drugi igrač 
                   
                    if (matrica[0, 2] == -1 && red == false) { clck(true, 0, 2); return sljedeci; }
                    if (matrica[1, 1] == 2) { clck(true, 0, 0); return sljedeci; }
                    return sljedeci;
                }

                if (brojac > 1)
                {
                    if (brojac == 3) //dio koda koji stvara formaciju "trokuta"
                    {
                        if (matrica[0, 1] == 2 && matrica[2, 0] == 2 && matrica[0, 2] == 1 && matrica[2, 2] == -1)
                        {
                            clck(true, 2, 2);
                            return sljedeci;
                        }
                        if (matrica[2, 0] == 2 && matrica[1, 2] == 2 && matrica[0, 2] == 1 && matrica[0, 0] == -1)
                        {
                            clck(true, 0, 0);
                            return sljedeci;
                        }
                        if (matrica[1, 1] == 2 && matrica[2, 2] == 2 && matrica[0, 0] == 1 && matrica[2, 0] == -1)
                        {
                            clck(true, 2, 0);
                            return sljedeci;
                        }
                        if (matrica[0, 2] == 2 && matrica[1, 0] == 2 && matrica[2, 0] ==1 && matrica[2, 2] == -1)
                        {
                            clck(true, 2, 2);
                            return sljedeci;
                        }

                    }


                    //STUPCI (DVA ELEMENTA  STUPCU)

                    suma = 0;
                    for (i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[j, i];
                        }

                        if (suma == 1 || suma == 3)
                        {
                            if (matrica[0, i] == -1) { clck(true, 0, i);}
                            if (matrica[1, i] == -1) { clck(true, 1, i);}
                            if (matrica[2, i] == -1) { clck(true, 2, i); }
                            return sljedeci;
                        }


                    }

                    suma = 0;
                    for (i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[j, i];
                        }


                        if (suma == 3)
                        {
                            if (matrica[0, i] == -1) { clck(true, 0, i); }
                            if (matrica[1, i] == -1) { clck(true, 1, i); }
                            if (matrica[2, i] == -1) { clck(true, 2, i); }
                            return sljedeci;
                        }

                    }




                    // prije svakoga ulaska u petlju sumu postavljamo na nulu, 
                    //kako nam se nebi povukla suma iz neke od prethodih petlji

                    //RETCI
                    suma = 0;
                    for (i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[i, j];
                           
                        }
                        if (suma == 1)
                        {

                            if (i == 0)
                            {
                                if (matrica[0, 0] == -1) { clck(true, 0, 0); }
                                if (matrica[0, 1] == -1) { clck(true, 0, 1); }
                                if (matrica[0, 2] == -1) { clck(true, 0, 2); }
                            }

                            if (i == 1)
                            {
                                if (matrica[1, 0] == -1) { clck(true, 1, 0); }
                                if (matrica[1, 1] == -1) { clck(true, 1, 1); }
                                if (matrica[1, 2] == -1) { clck(true, 1, 2); }
                            }

                            if (i == 2)
                            {
                                if (matrica[2, 0] == -1) { clck(true, 2, 0); }
                                if (matrica[2, 1] == -1) { clck(true, 2, 1); }
                                if (matrica[2, 2] == -1) { clck(true, 2, 2); }
                            }



                            return sljedeci;
                        }
                       
                    }


                    suma = 0;
                    for (i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[i, j];

                        }
                      
                        if (suma == 3)
                        {

                            if (i == 0)
                            {
                                if (matrica[0, 0] == -1) { clck(true, 0, 0); }
                                if (matrica[0, 1] == -1) { clck(true, 0, 1); }
                                if (matrica[0, 2] == -1) { clck(true, 0, 2); }
                            }

                            if (i == 1)
                            {
                                if (matrica[1, 0] == -1) { clck(true, 1, 0); }
                                if (matrica[1, 1] == -1) { clck(true, 1, 1); }
                                if (matrica[1, 2] == -1) { clck(true, 1, 2); }
                            }

                            if (i == 2)
                            {
                                if (matrica[2, 0] == -1) { clck(true, 2, 0); }
                                if (matrica[2, 1] == -1) { clck(true, 2, 1); }
                                if (matrica[2, 2] == -1) { clck(true, 2, 2); }
                            }


                            return sljedeci;
                        }
                    }


                    //SPOREDNA DIJAGONALA
                    suma = 0;
                    for (int i = 0; i < 3; i++)   //petlja koja računa sumu sporedne dijagonale
                    {
                        suma = suma + matrica[i, 3 - 1 - i];    
                    }

                    if (suma ==1 )
                    {   
                        if (matrica[0, 2] == -1) { clck(true, 0, 2); }
                        if (matrica[1, 1] == -1) { clck(true, 1, 1); }
                        if (matrica[2, 0] == -1) { clck(true, 2, 0); }
                        return sljedeci;            
                    }

                    if (suma == 3 )
                    {
                        if (matrica[0, 2] == -1) { clck(true, 0, 2); }
                        if (matrica[1, 1] == -1) { clck(true, 1, 1); }
                        if (matrica[2, 0] == -1) { clck(true, 2, 0); }
                        return sljedeci;    


                    }
                               



                    // GLAVNA DIJAGONALA
                    suma = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        suma = suma + matrica[i, i];
                    }

                    if (suma == 1 )
                    {
                        for (i = 0; i < 3; i++)
                        {
                            if (matrica[i, i] == -1)
                            {
                                 matrica[i, i] = 1; clck(true, i, i); return sljedeci;
                            }
                        }
                        
                    }

                    if (suma == 3)
                    {
                        for (i = 0; i < 3; i++)
                        {
                            if (matrica[i, i] == -1)
                            {
                                matrica[i, i] = 1; clck(true, i, i); return sljedeci;
                            }
                        }

                    }

                   

                    //STUPCI (SAMO JEDAN ELEMENT)
                    suma = 0;
                    for (i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[j, i];
                        }

                        if (suma == -1)
                        {
                            if (i == 0)
                            {

                                if (matrica[0, i] == 1) { clck(true, 1, 0); return sljedeci; }
                                if (matrica[1, i] == 1) { clck(true, 2, 0); return sljedeci; }
                                if (matrica[2, i] == 1) { clck(true, 0, 0); return sljedeci; }
                                return sljedeci;
                            }
                            if (i == 1)
                            {
                                if (matrica[0, i] == 1) { clck(true, 1, 1); return sljedeci; }
                                if (matrica[1, i] == 1) { clck(true, 2, 1); return sljedeci; }
                                if (matrica[2, i] == 1) { clck(true, 0, 1); return sljedeci; }
                                return sljedeci;
                            }
                            if (i == 2)
                            {
                                if (matrica[0, i] == 1) { clck(true, 1, 2); return sljedeci; }
                                if (matrica[1, i] == 1) { clck(true, 2, 2); return sljedeci; }
                                if (matrica[2, i] == 1) { clck(true, 0, 2); return sljedeci; }
                                return sljedeci;
                            }
                        }

                    }
                    //SPOREDNA DIJAGONALA (SAMO JEDAN ELEMENT)

                    suma = 0;
                    for (int i = 0; i < 3; i++)   //petlja koja računa sumu sporedne dijagonale
                    {
                        suma = suma + matrica[i, 3 - 1 - i];
                    }
                    if (suma == -1)   // ako je pounjeno jedno mjesto u sporednoj dijagonali, popuni prvo mjesto do njega
                    {
                        if (matrica[0, 2] == 1) { clck(true, 1, 1); return sljedeci; }
                        if (matrica[1, 1] == 1) { clck(true, 2, 0); return sljedeci; }
                        if (matrica[2, 0] == 1) { clck(true, 0, 2); return sljedeci; }
                        return sljedeci;
                    }

                    // GLAVNA DIJAGONALA
                    suma = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        suma = suma + matrica[i, i];
                    }

                    if (suma == -1)    
                    {
                        if (matrica[0, 0] == 1) { clck(true, 2, 2); return sljedeci; }
                        if (matrica[1, 1] == 1) { clck(true, 0, 0); return sljedeci; }
                        if (matrica[2, 2] == 1) { clck(true, 1, 1); return sljedeci; }
                    }

                    //ZA RETKE

                    suma = 0;   
                    for (int i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[i, j];
                        }

                        if (suma == -1)
                        {
                            if (i == 0)
                            {
                                if (matrica[i, 0] == 1) { clck(true, 0, 1); return sljedeci; }
                                if (matrica[i, 1] == 1) { clck(true, 0, 2); return sljedeci; }
                                if (matrica[i, 2] == 1) { clck(true, 0, 0); return sljedeci; }
                            }
                            if (i == 1)
                            {
                                if (matrica[i, 0] == 1) { clck(true, 1, 1); return sljedeci; }
                                if (matrica[i, 1] == 1) { clck(true, 1, 2); return sljedeci; }
                                if (matrica[i, 2] == 1) { clck(true, 1, 0); return sljedeci; }
                            }
                            if (i == 2)
                            {
                                if (matrica[i, 0] == 1) { clck(true, 2, 1); return sljedeci; }
                                if (matrica[i, 1] == 1) { clck(true, 2, 2); return sljedeci; }
                                if (matrica[i, 2] == 1) { clck(true, 2, 0); return sljedeci; }
                            }
                        }
                    }

                    //AKO NITI JEDAN UVIJET NIJE ISPUNJE 


                    for (int i = 0;  i<3;  i++) 
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (matrica[i,j] == -1) { clck(true, i, j); return sljedeci; }
                        }
                    }

                }
                
            }
            

            if (pv == false)
            {
                
                sljedeci = true; // slijedeći igrac je X
                if (brojac == 0) { clck(false, 1, 1); return sljedeci;}
                if (brojac == 1)
                {

                    //racunalo započinje igru
                    if (red == true && matrica[1, 1] != 1) { clck(false, 1, 1); return sljedeci; }

                    if (matrica[0, 0] == 1) { clck(false, 2, 2); return sljedeci; }
                    if (matrica[2, 2] == 1) { clck(false, 0, 0); return sljedeci; }

                    if (matrica[0, 2] == 1) { clck(false, 2, 0); return sljedeci; }
                    if (matrica[2, 0] == 1) { clck(false, 0, 2); return sljedeci; }

                    // računalo igra kao drugi igrač 

                    if (matrica[0, 2] == -1 && red == false) { clck(false, 0, 2); return sljedeci; }
                    if (matrica[1, 1] == 1) { clck(false, 0, 0); return sljedeci; }
                    return sljedeci;


                }




                if (brojac > 1)
                {

                    if (brojac == 3)
                    {
                        if (matrica[0, 1] == 1 && matrica[2, 0] == 1 && matrica[0, 2] == 2 && matrica[2, 2] == -1)
                        {
                            clck(false, 2, 2);
                            return sljedeci;
                        }

                        if (matrica[2, 0] == 1 && matrica[1, 2] == 1 && matrica[0, 2] == 2 && matrica[0, 0] == -1)
                        {
                            clck(false, 0, 0);
                            return sljedeci;
                        }


                        if (matrica[1, 1] == 1 && matrica[2, 2] == 1 && matrica[0, 0] == 2 && matrica[2, 0] == -1)
                        {
                            clck(false, 2, 0);
                            return sljedeci;
                        }

                        if (matrica[0, 2] == 1 && matrica[1, 0] == 1 && matrica[2, 0] == 2 && matrica[2, 2] == -1)
                        {
                            clck(false, 2, 2);
                            return sljedeci;
                        }
                    }



                    //STUPCI (DVA ELEMENTA  STUPCU)
                    suma = 0;
                    for (i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[j, i];
                        }

                        if (suma == 3)
                        {
                            if (matrica[0, i] == -1) { clck(false, 0, i); }
                            if (matrica[1, i] == -1) { clck(false, 1, i); }
                            if (matrica[2, i] == -1) { clck(false, 2, i); }
                            return sljedeci;
                        }
                        if (suma == 1)
                        {

                            if (matrica[0, i] == -1) { clck(false, 0, i); }
                            if (matrica[1, i] == -1) { clck(false, 1, i); }
                            if (matrica[2, i] == -1) { clck(false, 2, i); }
                            return sljedeci;
                        }
                    }

                    // prije svakoga ulaska u petlju sumu postavljamo na nulu, 
                    //kako nam se nebi povukla suma iz neke od prethodih petlji

                    //RETCI
                    suma = 0;
                    for (i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[i, j];
                        }
                        if (suma == 3)
                        {
                            if (i == 0)
                            {
                                if (matrica[0, 0] == -1) { clck(false, 0, 0); }
                                if (matrica[0, 1] == -1) { clck(false, 0, 1); }
                                if (matrica[0, 2] == -1) { clck(false, 0, 2); }
                            }

                            if (i == 1)
                            {
                                if (matrica[1, 0] == -1) { clck(false, 1, 0); }
                                if (matrica[1, 1] == -1) { clck(false, 1, 1); }
                                if (matrica[1, 2] == -1) { clck(false, 1, 2); }
                            }

                            if (i == 2)
                            {
                                if (matrica[2, 0] == -1) { clck(false, 2, 0); }
                                if (matrica[2, 1] == -1) { clck(false, 2, 1); }
                                if (matrica[2, 2] == -1) { clck(false, 2, 2); }

                            }
                            return sljedeci;
                        }

                        if (suma == 1)
                        {
                            if (i == 0)
                            {
                                if (matrica[0, 0] == -1) { clck(false, 0, 0); }
                                if (matrica[0, 1] == -1) { clck(false, 0, 1); }
                                if (matrica[0, 2] == -1) { clck(false, 0, 2); }
                            }

                            if (i == 1)
                            {
                                if (matrica[1, 0] == -1) { clck(false, 1, 0); }
                                if (matrica[1, 1] == -1) { clck(false, 1, 1); }
                                if (matrica[1, 2] == -1) { clck(false, 1, 2); }
                            }

                            if (i == 2)
                            {
                                if (matrica[2, 0] == -1) { clck(false, 2, 0); }
                                if (matrica[2, 1] == -1) { clck(false, 2, 1); }
                                if (matrica[2, 2] == -1) { clck(false, 2, 2); }

                            }
                            return sljedeci;
                        }
                    }

                    //SPOREDNA DIJAGONALA
                    suma = 0;
                    for (int i = 0; i < 3; i++)   //petlja koja računa sumu sporedne dijagonale
                    {
                        suma = suma + matrica[i, 3 - 1 - i];
                    }

                    if (suma == 3)
                    {
                        if (matrica[0, 2] == -1) { clck(false, 0, 2); }
                        if (matrica[1, 1] == -1) { clck(false, 1, 1); }
                        if (matrica[2, 0] == -1) { clck(false, 2, 0); }
                        return sljedeci;
                    }

                    if (suma == 1)
                    { 
                        if (matrica[0, 2] == -1) { clck(false, 0, 2); }
                        if (matrica[1, 1] == -1) { clck(false, 1, 1); }
                        if (matrica[2, 0] == -1) { clck(false, 2, 0); }
                        return sljedeci;             
                    }



                    // GLAVNA DIJAGONALA
                    suma = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        suma = suma + matrica[i, i];
                    }

                    if ( suma == 3)
                    {
                        for (i = 0; i < 3; i++)
                        {
                            if (matrica[i, i] == -1)
                            {
                                matrica[i, i] = 2; clck(false, i, i); return sljedeci;
                            }
                        }

                    }


                    if (suma == 1)
                    {
                        for (i = 0; i < 3; i++)
                        {
                            if (matrica[i, i] == -1)
                            {
                                matrica[i, i] = 2; clck(false, i, i); return sljedeci;
                            }
                        }

                    }


                    //STUPCI (SAMO JEDAN ELEMENT)
                    suma = 0;
                    for (i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[j, i];
                        }

                        if (suma == 0)
                        {
                            if (i == 0)
                            {

                                if (matrica[0, i] == 2) { clck(false, 1, 0); return sljedeci; }
                                if (matrica[1, i] == 2) { clck(false, 2, 0); return sljedeci; }
                                if (matrica[2, i] == 2) { clck(false, 0, 0); return sljedeci; }
                                return sljedeci;
                            }
                            if (i == 1)
                            {
                                if (matrica[0, i] == 2) { clck(false, 1, 1); return sljedeci; }
                                if (matrica[1, i] == 2) { clck(false, 2, 1); return sljedeci; }
                                if (matrica[2, i] == 2) { clck(false, 0, 1); return sljedeci; }
                                return sljedeci;
                            }
                            if (i == 2)
                            {
                                if (matrica[0, i] == 2) { clck(false, 1, 2); return sljedeci; }
                                if (matrica[1, i] == 2) { clck(false, 2, 2); return sljedeci; }
                                if (matrica[2, i] == 2) { clck(false, 0, 2); return sljedeci; }
                                return sljedeci;
                            }
                        }

                    }
                    //SPOREDNA DIJAGONALA (SAMO JEDAN ELEMENT)

                    suma = 0;
                    for (int i = 0; i < 3; i++)   //petlja koja računa sumu sporedne dijagonale
                    {
                        suma = suma + matrica[i, 3 - 1 - i];
                    }
                    if (suma == 0)   // ako je pounjeno jedno mjesto u sporednoj dijagonali, popuni prvo mjesto do njega
                    {
                        if (matrica[0, 2] == 2) { clck(false, 1, 1); return sljedeci; }
                        if (matrica[1, 1] == 2) { clck(false, 2, 0); return sljedeci; }
                        if (matrica[2, 0] == 2) { clck(false, 0, 2); return sljedeci; }
                        return sljedeci;
                    }

                    // GLAVNA DIJAGONALA (samo jedan element)
                    suma = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        suma = suma + matrica[i, i];
                    }

                    if (suma == 0)
                    {
                        if (matrica[0, 0] == 2) { clck(false, 2, 2); return sljedeci; }
                        if (matrica[1, 1] == 2) { clck(false, 0, 0); return sljedeci; }
                        if (matrica[2, 2] == 2) { clck(false, 1, 1); return sljedeci; }
                    }

                    //ZA RETKE (samo jeda element)

                    suma = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        suma = 0;
                        for (j = 0; j < 3; j++)
                        {
                            suma = suma + matrica[i, j];
                        }

                        if (suma == 0)
                        {
                            if (i == 0)
                            {
                                if (matrica[i, 0] == 2) { clck(false, 0, 1); return sljedeci; }
                                if (matrica[i, 1] == 2) { clck(false, 0, 2); return sljedeci; }
                                if (matrica[i, 2] == 2) { clck(false, 0, 0); return sljedeci; }
                            }
                            if (i == 1)
                            {
                                if (matrica[i, 0] == 2) { clck(false, 1, 1); return sljedeci; }
                                if (matrica[i, 1] == 2) { clck(false, 1, 2); return sljedeci; }
                                if (matrica[i, 2] == 2) { clck(false, 1, 0); return sljedeci; }
                            }
                            if (i == 2)
                            {
                                if (matrica[i, 0] == 2) { clck(false, 2, 1); return sljedeci; }
                                if (matrica[i, 1] == 2) { clck(false, 2, 2); return sljedeci; }
                                if (matrica[i, 2] == 2) { clck(false, 2, 0); return sljedeci; }
                            }
                        }
                    }

                    //AKO NITI JEDAN UVIJET NIJE ISPUNJE 


                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (matrica[i, j] == -1) { clck(false, i, j); return sljedeci; }
                        }
                    }


                }

            }

            return sljedeci;
        }

    }
}

